
<%--spécifie le langage utilisé etc --%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%--Intégration des balises JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- Bibliothèques de balises Spring --%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE html>
<html>
<head>
        <base href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/authenticate"%>" />
	<title>Login</title>
	 <meta charset="utf-8">
</head>
<body>

        <a href="${requestScope['javax.servlet.forward.request_uri']}?lang=fr">FRANCAIS</a>
        <a href="${requestScope['javax.servlet.forward.request_uri']}?lang=en">ENGLISH</a>

 
<div style="color:red;">
                <spring:hasBindErrors name="login-form">
                        <c:forEach var="err" items="${errors.allErrors}">
                                <c:out value="${err.field}" />
                                <c:out value="${err.defaultMessage}" />
                               <br/> 
                        </c:forEach>
                </spring:hasBindErrors>
        </div>
         

<h1>
		Authentification
</h1>

	<form:form method="post" action="check-login"
	modelAttribute="login-form">
	
	
	<form:label path="email">
	<spring:message code="login.lblEmail"/>
	</form:label>
	<form:input path="email"/>
	<!--a voir <form:errors path="email"></form:errors> -->
	<br/>
	
	<form:label path="password">
	<spring:message code="login.lblPassword"/>
	</form:label>
	<form:password path="password"/>
	<br/>
	
	<input type="submit" value="Se Connecter"/>
	</form:form>
	<div>${msg}</div>
</body>
</html>
