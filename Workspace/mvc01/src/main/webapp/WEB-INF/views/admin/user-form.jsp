
<%--spécifie le langage utilisé etc --%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%--Intégration des balises JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- Bibliothèques de balises Spring --%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE html>
<html>
<head>
        <base href="<%=
        request.getScheme()+"://"+request.getServerName()+":"
        + request.getServerPort()+request.getContextPath()+"/authenticate"
        %>" />
	<title>Formulaire</title>
	 <meta charset="utf-8">
</head>
<body>
<h1>
		${title}
</h1>

	<form:form method="post" action="admin/save"
	modelAttribute="u">
	
	<form:hidden path="id"/>
	
	<form:label path="name">Nom :</form:label>
	<form:input path="name"/>
	<br/>
	
	<form:label path="email">Email :</form:label>
	<form:input path="email"/>
	<br/>
	
	<c:choose>
	<c:when test="${isAdd}">
	<form:label path="password">Mot de passe :</form:label>
	<form:password path="password" showPassword="true"/>
	<br/>
	</c:when>
	<c:otherwise>
	<form:hidden path="password"/>
	<br/>
	</c:otherwise>
	</c:choose>
	<form:label path="admin">Admin :</form:label>
	<form:checkbox path="admin"/>
	<br/>
	<form:hidden path="id"/>
	<form:hidden path="version"/>
	
	<input type="submit" value="${buttonForm}"/>
	</form:form>
	<div>${msg}</div>
</body>
</html>
