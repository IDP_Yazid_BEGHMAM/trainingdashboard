<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<!--  la balise base sert � pr�fixer l'ensemble
        des liens et des images pour �viter des
        probl�mes de redirection -->
        <base href="<%=
        request.getScheme()+"://"+request.getServerName()+":"
        + request.getServerPort()+request.getContextPath()+"/admin/espace-admin"
        %>" />
        
	<title>Espace admin</title>
	 <meta charset="utf-8">
</head>
<body>
<h1>
		Espace administrateur
</h1>

	<p>Bonjour ${sessionScope.email }</p>
	<a href="disconnect" title="disconnect">Se d�connecter</a>
	
	<a href="users?page=1&max=30" title="gestion utilisateurs">Gestion des users</a>
</body>
</html>