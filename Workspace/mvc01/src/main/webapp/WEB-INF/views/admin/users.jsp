
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/admin/users"%>" />
<meta charset="ISO-8859-1">
<title>Users</title>
</head>
<body>
	<h1>Gestion utilisateurs</h1>
	<a href="users/add">Ajouter un utilisateur</a>
	<br />

	<fieldset>
		<legend>Recherche par nom :</legend>
		<form:form method="post" action="users" modelAttribute="u">
			<form:input path="name" />
			<input type="submit" value="Rechercher">
		</form:form>
	</fieldset>


	<c:choose>
		<c:when test="${resVide }">
			<p>Aucun r�sultat !</p>
		</c:when>
		<c:otherwise>
			<table>
				<tr>
					<th>Utilisateur</th>
					<th>Name</th>
					<th>E-mail</th>
					<th>Admin</th>
					<th>Actions</th>
				</tr>
				<c:forEach items="${liste}" var="user">
					<tr>
						<td><c:out value="${user.id}" /></td>
						<td><c:out value="${user.name}" /></td>
						<td><c:out value="${user.email}" /></td>
						<td><c:choose>
								<%--�quivalent du if/else --%>
								<c:when test="${user.admin}">Oui</c:when>
								<c:otherwise>Non</c:otherwise>
							</c:choose></td>
						<td><a href="users/update/${user.id}"
							title="Modifier ${user.name}"> Modifier</a> <a
							href="users/delete/${user.id}" title="Supprimer ${user.name}">
								Supprimer</a></td>
					</tr>
				</c:forEach>
			</table>
			<div>
			<c:if test="${page>1 }">
	<a href="users?page=${page-1}&max=${max}" title="Pr�cedent"> Pr�c</a>
	</c:if>
	<span>${page }</span>
	<c:if test="${suivExist }">
	<a href="users?page=${page+1}&max=${max}" title="Suivant"> Suiv</a>
	</c:if>
	
			</div>
		</c:otherwise>
	</c:choose>
	
	
	
	
	<a href="export-users" title="Exporter"> Export CSV
	</a>

	<h4>Import CSV</h4>
	<form:form method="post" action="upload-users" enctype="multipart/form-data">
	<input type="file" name="file"/>
	<br/>
	<input type="submit" value="Uploader"/>
	
	</form:form>
</body>
</html>