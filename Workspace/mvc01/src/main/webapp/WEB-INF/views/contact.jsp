
<%--spécifie le langage utilisé etc --%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%--Intégration des balises JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- Bibliothèques de balises Spring --%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Contact</title>
</head>
<body>

        <a href="${requestScope['javax.servlet.forward.request_uri']}?lang=fr">FRANCAIS</a>
        <a href="${requestScope['javax.servlet.forward.request_uri']}?lang=en">ENGLISH</a>

 
<div style="color:red;">
                <spring:hasBindErrors name="contact-form">
                        <c:forEach var="err" items="${errors.allErrors}">
                                <c:out value="${err.field}" />
                                <c:out value="${err.defaultMessage}" />
                               <br/> 
                        </c:forEach>
                </spring:hasBindErrors>
        </div>
         

<h1>
	<spring:message code="contact.lblTitle"/>
</h1>

	<form:form method="post" action="sendContactForm"
	modelAttribute="contact-form">
	
	
	<form:label path="nom">
	<spring:message code="contact.lblNom"/>
	</form:label>
	<form:input path="nom"/>
	<!--a voir <form:errors path="email"></form:errors> -->
	<br/>
	
	<form:label path="email">
	<spring:message code="contact.lblEmail"/>
	</form:label>
	<form:input path="email"/>
	<br/>
	
	<form:label path="typeDemande">
	<spring:message code="contact.lblTypeDemande"/>
	</form:label>
	<form:select path="typeDemande" items="${typesDemande}">
	</form:select>
	<br/>
	
	<form:label path="message">
	<spring:message code="contact.lblMessage"/>
	</form:label>
	<form:textarea path="message"/>
	<br/>
	
	<input type="submit" value="<spring:message code="contact.lblMessage"/>">
	
	</form:form>
	<div>${msg}</div>
</body>
</html>