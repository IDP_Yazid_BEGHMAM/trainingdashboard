
package tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.dawan.mvc01.beans.User;

public class Tools {

	// méthode qui exporte en csv une liste d'utilisateurs
	// visibilité mots-clé typeRetour nomMethode(paramètres)
	public static <T> void toCsv(String filePath, List<T> myList, List<String> columns, String separator)
			throws Exception {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath))) {
			if (myList != null && myList.size() > 0) {
				Field[] tab = myList.get(0).getClass().getDeclaredFields();
				StringBuilder ligneEntete = new StringBuilder();
				for (Field f : tab) {
					f.setAccessible(true);
					if (columns.contains(f.getName()))
						ligneEntete.append(f.getName()).append(separator);
				}
				bw.write(ligneEntete.toString().substring(0, ligneEntete.length() - 1));
				bw.newLine();
				for (T obj : myList) {
					StringBuilder line = new StringBuilder();
					for (Field f : tab) {
						if (columns.contains(f.getName()))
							line.append(f.get(obj).toString()).append(separator);
					}
					bw.write(line.toString().substring(0, line.length() - 1));
					bw.newLine();
				}
			}
		}
	}
	
	/**
	 * Import d'une csv
	 * @param filePath
	 * @return
	 * @throws Exception
	 */
	public static List<User> importCsv(String filePath) throws Exception{

		List<User> users=new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {

			reader.readLine();	//ligne d'entête
			String ligne=null;
			
			while((ligne=reader.readLine())!=null){	
				System.out.println("trim : "+ligne.trim());
				if (!ligne.trim().isEmpty()) {
					
					String[] infosUser=ligne.split(";");
					if (infosUser.length==3) {
						try {
							User u = new User();
							u.setName(infosUser[0]);
							u.setEmail(infosUser[1]);
							u.setAdmin(Boolean.parseBoolean(infosUser[2]));
							
							users.add(u);
						}
						catch(Exception ex) {
							//TODO Log
						}
					}
				}
			}
		}
		System.out.println("importcsv retour : "+users);
		return users;
	}

	public static void main(String[] args) {
		/*
		List<User> lu = new ArrayList<>();
		for (int i = 1; i < 10; i++) {
			lu.add(new User(i, "name" + i, "email" + i, "password" + i));
		}
		List<String> columns = Arrays.asList("name", "email");
		try {
			Tools.toCsv("users.csv", lu, columns, ";");
			System.out.println("export effectué");
		} catch (Exception e) {
			e.printStackTrace();
		}*/

		try {
			System.out.println(importCsv("C:/Users/yazid/Desktop/users.csv"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}