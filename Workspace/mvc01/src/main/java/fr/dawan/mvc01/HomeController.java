package fr.dawan.mvc01;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.dawan.mvc01.beans.User;
import fr.dawan.mvc01.beans.User.UserStatus;
import fr.dawan.mvc01.dao.UserDao;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	@Autowired
	private UserDao userDao;

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);

		return "home";
	}

	@GetMapping("/test/insert-data")
	public String insertData() {
		for (int i = 0; i < 50; i++) {
			User u = new User();
			u.setName("test" + i);
			u.setEmail("test" + i + "@dawan.fr");
			u.setPassword("test" + i);
			u.setCreationDate(new Date());
			u.setAdmin(true);
			u.setCurrentStatus(UserStatus.ACTIVE);
			userDao.insert(u);
			
			userDao.getHibernateTemplate().evict(u);	
			//a chaque fois qu'on stocke un objet via hibernate, celui-ci le garde en cache.
			//la méthode evict permet de supprimer l'objet du cache pour remédier aux 
			//problèmes notoires d'hibernate en terme de performance
		}
		return "home";
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

}
