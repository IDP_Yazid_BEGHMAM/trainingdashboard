package fr.dawan.mvc01.dao;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.dawan.mvc01.beans.User;

public class UserDaoOld {

	private static List <User> listeUtilisateurs;
	/*
	static {
		listeUtilisateurs=new ArrayList<>();

		User u1=new User(1,"u1name","email1","u1Pword");
		User u2=new User(2,"u2name","email2","u2Pword");
		User u3=new User(3,"u3name","email3","u3Pword");
		

		listeUtilisateurs.add(u1);
		listeUtilisateurs.add(u2);
		listeUtilisateurs.add(u3);
		
	}
	*/
	public static void setListe(List <User> plisteUtilisateurs) {
		listeUtilisateurs=plisteUtilisateurs;
	}
	public static List<User> findAll(){
		return listeUtilisateurs;
	}
	
	public static void insert(User u) {
		u.setId((long) (listeUtilisateurs.size()+1));
		listeUtilisateurs.add(u);
	}
	
	public static User findById(long id) {
		Optional <User> u = listeUtilisateurs.stream().filter(x->x.getId()==id).findAny();

		if (u.isPresent()) return u.get();
		else return null;
	}
	
	public static void update(User u) {
		User toChange=findById(u.getId());
		int pos=listeUtilisateurs.indexOf(toChange);
		if (pos!=-1) {
			listeUtilisateurs.set(pos, u);
		}
	}
	
	public static void remove(long id) {
		listeUtilisateurs.remove(findById(id));
	}
	
	public static List<User> findByName(String rech){
		return listeUtilisateurs.stream().filter(x->x.getName().contains(rech)).
				collect(Collectors.toList());
	}
	
	public static void export() {
		
	}
	
}
