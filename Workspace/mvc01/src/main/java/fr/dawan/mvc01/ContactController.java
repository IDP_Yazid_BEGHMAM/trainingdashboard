package fr.dawan.mvc01;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import fr.dawan.mvc01.formBeans.ContactForm;

@Controller
public class ContactController {

	@GetMapping("contact")
	public String contact(Model model) {
		ContactForm c = new ContactForm();
		model.addAttribute("contact-form", c);
		model.addAttribute("typesDemande", c.getTypeDemande());

		return "contact";
	}

	@PostMapping("/sendContactForm")
	public String sendContactForm(HttpServletRequest request, Model model, // Pour pouvoir passer des paramètres
			@Valid @ModelAttribute("contact-form") ContactForm form, // Formulaire envoyé
			BindingResult result) { // si erreurs, result sera remplie

		try {
			Email email = new SimpleEmail();
			email.setHostName("smtp.googlemail.com");
			email.setSmtpPort(465);
			email.setAuthenticator(new DefaultAuthenticator("username", "password"));
			email.setSSLOnConnect(true);
			email.setFrom(form.getEmail());
			email.setSubject(form.getTypeDemande().toString());
			email.setMsg(form.getMessage());
			email.addTo("yazidbeghmam@gmail.com");
			email.send();
			model.addAttribute("msgRetour", "message envoyé");

		} catch (

		Exception e) {
			e.printStackTrace();
			model.addAttribute("msgRetour", "Erreur: " + e.getMessage());
			model.addAttribute("contact-form", form);
		}

		// si erreurs de validation du formulaire(voir annotations dans LoginForm)
		if (result.hasErrors()) {
			model.addAttribute("errors", result);
			model.addAttribute("contact-form", form);
			return "contact";
		}

		return "redirect:/";
	}

}
