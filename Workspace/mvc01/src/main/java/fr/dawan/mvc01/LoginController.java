package fr.dawan.mvc01;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.dawan.mvc01.beans.User;
import fr.dawan.mvc01.dao.UserDao;
import fr.dawan.mvc01.formBeans.LoginForm;

@Controller
public class LoginController {

	@Autowired
	private UserDao userDao;		
	//la variable doit être la même que dans le servlet-context.xml
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.GET)
	// ou @GetMapping("/authenticate")
	public String showLogin(Model model) {
		// L'objet loginForm sert à initialiser le formulaire et à récupérer
		// les données saisies une fois le formulaire validé
		LoginForm f = new LoginForm("admin@dawan.fr", "admin");
		model.addAttribute("login-form", f);

		return "login"; // WEB-INF/views/login.jsp
	}

	@PostMapping("/check-login")
	public String checkLogin(HttpServletRequest request,
			Model model,				//Pour pouvoir passer des paramètres
			@Valid @ModelAttribute("login-form") LoginForm form,	//Formulaire envoyé
			BindingResult result) {		//si erreurs, result sera remplie
		
//		String cible = "espace-admin";
		String cible="redirect:/admin/espace-admin";

		//si erreurs de validation du formulaire(voir annotations dans LoginForm)
		if (result.hasErrors()) {
            model.addAttribute("errors", result);
            model.addAttribute("login-form",form);
            return "login";
		}
		
		
		User u = userDao.findByEmail(form.getEmail());
		
		
		if (u!=null && u.getPassword().equals(form.getPassword())) {
			//Ajout d'une variable enregistrée dans la session
			request.getSession().setAttribute("userId", form.getEmail());
			request.getSession().setAttribute("email", form.getEmail());
		}else {
			model.addAttribute("msg","Erreur d'authentification");
			model.addAttribute("login-form",form);
			cible="login";
		}
		
		return cible;
	}
	

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

}
