package fr.dawan.mvc01.formBeans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class ContactForm {

	@NotEmpty
	private String nom;

	@NotEmpty
	@Pattern(regexp = "\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b", message = "Email invalide")
	private String email;

	@NotEmpty
	private List<String> typeDemande;
	

	@NotEmpty
	private String message;

	public ContactForm() {
		typeDemande=new ArrayList<>();
		typeDemande.add("op1");
		typeDemande.add("op2");
		typeDemande.add("op3");
	}

	
	public ContactForm(@NotEmpty String nom,
			@NotEmpty @Pattern(regexp = "\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b", message = "Email invalide") String email,
			@NotEmpty List<String> typeDemande, @NotEmpty String message) {
		super();
		this.nom = nom;
		this.email = email;
		this.typeDemande = typeDemande;
		this.message = message;
	}


	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<String> getTypeDemande() {
		return typeDemande;
	}


	public void setTypeDemande(List<String> typeDemande) {
		this.typeDemande = typeDemande;
	}


	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
