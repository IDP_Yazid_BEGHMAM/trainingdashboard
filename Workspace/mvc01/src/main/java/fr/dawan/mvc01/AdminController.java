package fr.dawan.mvc01;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import fr.dawan.mvc01.beans.User;
import fr.dawan.mvc01.dao.UserDao;
import fr.dawan.mvc01.formBeans.LoginForm;
import tools.Tools;

@Controller
public class AdminController {


	@Autowired
	private UserDao userDao;	
	//la variable doit être la même que dans le servlet-context.xmls	
	
	@RequestMapping(value = "/admin/espace-admin", method = RequestMethod.GET)
	// l'url est l'url que je crée et sera l'url de ma page web
	// ou @GetMapping("/authenticate")
	public String accueilAdmin(Model model) {
		// L'objet loginForm sert à initialiser le formulaire et à récupérer
		// les données saisies une fois le formulaire validé
		LoginForm f = new LoginForm("admin@dawan.fr", "admin");
		model.addAttribute("login-form", f);

		return "admin/espace-admin"; // WEB-INF/views/admin/espace-admin.jsp
	}

	@GetMapping("admin/users")
	public String afficheUsers(Model model,@RequestParam(name="page", required=false)Integer page
										  , @RequestParam(name="max", required=false)Integer max) {
		if (page==null)page=1;
		if (max==null)max=30;
		
		int start=(page-1)*max;
		List<User> liste = userDao.findAll(start,max);
		
		model.addAttribute("page",page);
		model.addAttribute("max",max);
		model.addAttribute("suivExist",(page*max)<userDao.nbUsers());

		model.addAttribute("liste", liste);
		model.addAttribute("u", new User());
		model.addAttribute("isAdd",true);
		return "admin/users";
	}

	@GetMapping("/admin/users/{action}/{id}")
	public String update(HttpServletRequest req, @PathVariable("action") String action, @PathVariable("id") long id,
			Model model) {

		String cible = "redirect:/admin/users";

		if (action.equals("delete")) {
			userDao.remove(id);
		} else if (action.equals("update")) {
			User u = userDao.findById(id);
			model.addAttribute("u", u);
			model.addAttribute("title", "Modification de l'utilisateur " + u.getId());
			model.addAttribute("buttonForm", "Modifier");
			model.addAttribute("isAdd",false);
			cible = "admin/user-form"; // cible = WEB-INF/views/admin/user-form.jsp
		}

		return cible;
	}

	@GetMapping("/admin/users/add")
	public String add(Model model) {
		model.addAttribute("u", new User());
		model.addAttribute("title", "Ajout d'un utilisateur");
		model.addAttribute("buttonForm", "Ajouter");
		model.addAttribute("isAdd",true);

		return "admin/user-form";
	}

	@PostMapping("/admin/save")
	public String save(@ModelAttribute("u") User u, BindingResult result) {

		String cible = "redirect:/admin/users";

		if (u.getId()==null || u.getId() == 0) { // insertion
			userDao.insert(u);
		} else { // Modification
			userDao.update(u);
		}
		return cible;
	}

	@PostMapping("/admin/users")
	public String search(@ModelAttribute("u") User u, Model model, BindingResult result) {

		String cible = "admin/users";

		List<User> liste = userDao.findByName(u.getName());

		model.addAttribute("resVide", (liste == null || liste.size() == 0));
		model.addAttribute("liste", liste);
		model.addAttribute("u", u);

		return cible;
	}

	@GetMapping("/admin/export-users")
	public void exportCsv(HttpServletResponse response) throws Exception {
		response.setContentType("text/csv");
		response.setHeader("Content-Disposition", // une requete est en fait un header avec des paramètres, dont
													// content-disposition
				"attachment;filename=\"users.csv\"");// voir dans network en mode debug(chrome)
		ServletOutputStream out = response.getOutputStream();
		// on peut avoir response.getWriter() qui écrit en caractères
		out.write("Name;Email;Admin".getBytes());
		out.write("\n".getBytes());
		for (User u : userDao.findAll()) {
			StringBuilder ligne = new StringBuilder();
			ligne.append(u.getName()).append(";");
			ligne.append(u.getEmail()).append(";");
			ligne.append(u.isAdmin());
			ligne.append("\n");
			out.write(ligne.toString().getBytes());

		}
		out.close();
	}

	@PostMapping("/admin/upload-users")
	public String uploadCsv(Model model, HttpServletRequest request, @RequestParam("file") MultipartFile file)
			throws IOException {

		if (!file.isEmpty()) {
			try {
				byte[] contentBytes = file.getBytes(); // on récupère un tableau de bytes(valeurs binaires)
				String dirPath = "C:/uploads";
				// String dirPath = request.getServletContext().getRealPath("")+"/uploads";
				// //chemin relatif
				File dir = new File(dirPath);
				if (!dir.exists())
					dir.mkdirs();

				String filePath = dir.getAbsolutePath() + File.separator + file.getOriginalFilename();
				try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath))) {
					bos.write(contentBytes);
					
				}
				
				
				List<User> myImportedList = Tools.importCsv(filePath);
				for (User x : myImportedList) {
					userDao.insert(x);
				}
				new File(filePath).delete();// Files.delete(Paths.get(filePath));
				
				
				
				

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "redirect:/admin/users";
	}
	
	@GetMapping("admin/disconnect")
	public String discoonect(HttpServletRequest request) {
		request.getSession().invalidate();
		return "redirect:/authenticate";
	}


	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
}
